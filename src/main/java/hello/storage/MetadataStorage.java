/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hello.storage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author denis
 */
public class MetadataStorage implements StorageService{

    private final Path rootLocation;
//{ file:"book.pdf", ext:"pdf",s ize: "3mg" }
    private static final String FORMAT = "{ file:\"%s\", ext:\"%s\",size:\"%d\" }";
    private static final String EXTENSION = ".txt";
    
    @Autowired
    public MetadataStorage(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getPath());
    }

    @Override
    public void store(MultipartFile file) {
        String name = StringUtils.cleanPath(file.getOriginalFilename());
        String extension = getExtension(name);
        Long size = file.getSize();

        if (name.contains("..")) {
            throw new StorageException("Cannot store file with relative path outside current directory: " + name);
        }
     
        try {
            Files.write(Paths.get(rootLocation.toString() + File.separator + name + EXTENSION), String.format(FORMAT, name,extension,size).getBytes());
        } catch (IOException e) {
            throw new StorageException("IO Exception in storage", e);
        }
    }
    
    private String getExtension(String filename)
    {
        // If fileName do not contain "." or starts with "." then it is not a valid file
        if(filename.contains(".") && filename.lastIndexOf(".")!= 0)
        {
               return filename.substring(filename.lastIndexOf(".")+1);
        }
        return "";
    }

    @Override
    public void init() {
        try {
            Files.createDirectories(rootLocation);
        }
        catch (IOException e) {
            throw new StorageException("Could not initialize storage", e);
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }
}
