package hello.storage;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;

@ConfigurationProperties("storage")
public class StorageProperties {    
    private String storageType;
    private String path;
    private String endpointURL;
    private String bucketName;
    private String accessKey;
    private String secretKey;
    
    
    @Bean
    @ConditionalOnProperty(name = "storage.storageType", havingValue = "Metadata", matchIfMissing = true)
    public StorageService getMetadataStorage()
    {
        return new MetadataStorage(this);
    }
    
    @Bean
    @ConditionalOnProperty(name = "storage.storageType", havingValue = "FileSystem")
    public StorageService getFileSystemStorage()
    {
        return new FileSystemStorage(this);
    }
    
    @Bean
    @ConditionalOnProperty(name = "storage.storageType", havingValue = "s3")
    public StorageService getS3Storage()
    {
        return new AmazonStorage(this);
    }
    
    /**
     * @return the storageType
     */
    public String getStorageType() {
        return storageType;
    }

    /**
     * @param storageType the storageType to set
     */
    public void setStorageType(String storageType) {
        this.storageType = storageType;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return the endpointURL
     */
    public String getEndpointURL() {
        return endpointURL;
    }

    /**
     * @param endpointURL the endpointURL to set
     */
    public void setEndpointURL(String endpointURL) {
        this.endpointURL = endpointURL;
    }

    /**
     * @return the bucketName
     */
    public String getBucketName() {
        return bucketName;
    }

    /**
     * @param bucketName the bucketName to set
     */
    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    /**
     * @return the accessKey
     */
    public String getAccessKey() {
        return accessKey;
    }

    /**
     * @param accessKey the accessKey to set
     */
    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    /**
     * @return the secretKey
     */
    public String getSecretKey() {
        return secretKey;
    }

    /**
     * @param secretKey the secretKey to set
     */
    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

}
